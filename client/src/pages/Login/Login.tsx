import { useState } from 'react';
import { useProfileStore } from '@/stores/profile';
import { useNavigate } from 'react-router-dom';
import { userLogin } from '@/api/user';

export const Login = () => {
  const { setProfile } = useProfileStore();
  const navigate = useNavigate();

  const [email, setEmail] = useState('');

  const handleLogin = async (event: any) => {
    event.preventDefault();
    try {
      const { data } = await userLogin({ email });
      setProfile(data.data);
      navigate('/chat');
    } catch (e) {
      console.log('e');
    }
  };

  return (
    <div className="w-full flex justify-center">
      <form className="w-[400px]" onSubmit={handleLogin}>
        <input
          className="w-full border border-gray-200 p-2 mb-2"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <button type="submit" className="w-full bg-gray-800 text-white p-2">
          Login
        </button>
      </form>
    </div>
  );
};
