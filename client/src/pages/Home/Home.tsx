import React, { useEffect } from 'react';
import { Channels, Chat } from '@/components';
import { useChatStore } from '@/stores/chat';
import socketClient from 'socket.io-client';
import { ChannelInfo } from '@/components/ChannelInfo';
import { useProfileStore } from '@/stores/profile';

const SERVER = 'http://localhost:8080';

const socket = socketClient(SERVER, { path: '/api/chat' });

export const Home: React.FC = () => {
  const {
    setSocket,
    getChannels,
    getDetailedChannelInfo,
    setMessages,
    activeChannel,
    messages,
    setActiveChannel
  } = useChatStore();
  const { profile } = useProfileStore();

  useEffect(() => {
    setSocket(socket);
    socket.on('connection', () => {
      console.log(`I'm connected with the back-end`);
    });
    socket.on('channel-join', (channel) => {
      getDetailedChannelInfo(channel.id);
    });
    socket.on('channel-leave', (channelId) => {
      getDetailedChannelInfo(channelId);
    });
    socket.on('channel-create', async () => {
      await getChannels();
    });
  }, []);

  useEffect(() => {
    socket.on('message', (message) => {
      setMessages([...messages, message]);
      activeChannel && getDetailedChannelInfo(activeChannel);
    });
    socket.on('channel-remove', async (channelId) => {
      await getChannels();
      if (channelId === activeChannel) {
        setActiveChannel(null);
      }
    });
    socket.on('channel-remove-user', async ({ userId, channelId }) => {
      activeChannel && getDetailedChannelInfo(activeChannel);
      socket.emit('channel-leave-user', channelId);
      if (userId === profile?.id && channelId === activeChannel) {
        setActiveChannel(null);
      }
    });

    return () => {
      socket.off('message');
      socket.off('channel-remove');
      socket.off('channel-remove-user');
    };
  }, [messages, activeChannel, profile]);

  return (
    <div className="grid grid-cols-5 gap-4">
      <div className="border border-gray-200 p-2 rounded-lg">
        <Channels />
      </div>
      <div className="col-span-3 h-[calc(100vh-80px)] border border-gray-200 p-2 rounded-lg">
        <Chat />
      </div>
      <div className="p-2 border border-gray-200 rounded-lg">
        <ChannelInfo />
      </div>
    </div>
  );
};
