import { Routes, Route } from 'react-router-dom';
import { routes } from '@/routes';
import { AppRoute } from '@/types';

import './setupAxios';

function renderRoute(_props: AppRoute) {
  // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
  const { menu, routes, ...props } = _props;
  const children = routes?.map((route) => renderRoute(route)) ?? <></>;

  return <Route {...props}>{children}</Route>;
}

function App() {
  return (
    <Routes>
      {routes.map((route) => {
        return renderRoute(route);
      })}
    </Routes>
  );
}

export default App;
