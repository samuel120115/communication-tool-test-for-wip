import React from 'react';
import { Channel } from '@/types';
import { useChatStore } from '@/stores/chat';
import { useProfileStore } from '@/stores/profile';
import clsx from 'clsx';
import { removeChannel } from '@/api/chat';
import DropDownMenu from '@/layout/DropDownMenu';
import { EllipsisHorizontalIcon } from '@heroicons/react/20/solid';

const items = [
  {
    label: 'Remove channel',
    value: 'delete'
  }
];

type Props = {
  channel: Channel;
  onClickChannel: (channel: Channel) => void;
};

export const ChannelItem: React.FC<Props> = ({ channel, onClickChannel }) => {
  const { activeChannel, socket, getChannels, setActiveChannel } =
    useChatStore();
  const { profile } = useProfileStore();

  const handleRemove = async (value: string) => {
    if (value === 'delete') {
      try {
        await removeChannel(channel.id);
        socket?.emit('channel-remove', channel.id);
        await getChannels();
        if (channel.id === activeChannel) {
          setActiveChannel(null);
        }
      } catch (e) {
        console.log(e);
      }
    }
  };

  return (
    <div
      className={clsx(
        'group relative p-2 border border-gray-200 mb-2 rounded-lg cursor-pointer flex items-center',
        activeChannel === channel.id && 'bg-gray-800 text-white'
      )}
      onClick={() => onClickChannel(channel)}>
      <p className="flex-1">{channel.name}</p>
      {channel.private && (
        <span className="text-xs p-1 mx-2 rounded-md border border-gray-300 bg-gray-100 text-black">
          Private
        </span>
      )}

      {profile?.role === 'admin' && (
        <DropDownMenu
          options={items}
          icon={<EllipsisHorizontalIcon />}
          onClickMenu={handleRemove}
        />
      )}
    </div>
  );
};
