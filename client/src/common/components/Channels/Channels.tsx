import React, { useEffect, useState } from 'react';
import { ChannelItem } from '@/components';
import { useChatStore } from '@/stores/chat';
import { useProfileStore } from '@/stores/profile';
import { CreateChannelModal } from '@/components/CreateChannelModal';
import { ConfirmPasswordModal } from '@/components/ConfirmPasswordModal';
import { Channel } from '@/types';

export const Channels = () => {
  const {
    socket,
    setActiveChannel,
    getChannels,
    getMessages,
    channels,
    activeChannel
  } = useChatStore();
  const { profile } = useProfileStore();
  const [isOpenCreateModal, setIsOpenCreateModal] = useState<boolean>(false);
  const [isOpenConfirmPasswordModal, setIsConfirmPasswordModal] =
    useState<boolean>(false);
  const [tempChannel, setTempChannel] = useState<Channel | null>(null);

  useEffect(() => {
    getChannels();
  }, []);

  // useEffect(() => {
  //   if (channels?.length) {
  //     handleSelectChannel(channels[0].id);
  //     await getDetailedChannelInfo(channelId);
  //   }
  // }, [channels]);

  const handleClickChannel = async (channel: Channel) => {
    if (channel.private) {
      setIsConfirmPasswordModal(true);
      setTempChannel(channel);
    } else {
      await handleSelectChannel(channel);
    }
  };

  const handleSelectChannel = async (channel: Channel) => {
    if (socket) {
      activeChannel &&
        socket.emit('channel-leave', {
          channelId: activeChannel,
          userId: profile?.id
        });
      socket.emit('channel-join', {
        channelId: channel.id,
        userId: profile?.id
      });
      setActiveChannel(channel.id);
      await getMessages(channel.id);
    }
  };

  const handleClosePasswordModal = (status: boolean) => {
    if (status && tempChannel) {
      handleSelectChannel(tempChannel);
    }
    setIsConfirmPasswordModal(false);
    setTempChannel(null);
  };

  return (
    <div>
      <div className="flex justify-end py-2">
        <button
          className="py-1 px-2 rounded-md border border-gray-300 bg-gray-200 text-sm"
          onClick={() => setIsOpenCreateModal(true)}>
          Add
        </button>
      </div>
      {channels.map((channel, index) => (
        <ChannelItem
          key={index}
          channel={channel}
          onClickChannel={handleClickChannel}
        />
      ))}

      <CreateChannelModal
        isOpen={isOpenCreateModal}
        onClose={() => setIsOpenCreateModal(false)}
      />
      <ConfirmPasswordModal
        isOpen={isOpenConfirmPasswordModal}
        channelId={tempChannel?.id}
        onClose={handleClosePasswordModal}
      />
    </div>
  );
};
