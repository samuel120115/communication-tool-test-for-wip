import React, { useState } from 'react';
import { MessageItem } from '@/components/Message';
import { useChatStore } from '@/stores/chat';
import { useProfileStore } from '@/stores/profile';

export const Chat = () => {
  const { socket, activeChannel, messages, setMessages } = useChatStore();
  const { profile } = useProfileStore();

  const [message, setMessage] = useState<string>('');
  const handleSubmit = (event: any) => {
    event.preventDefault();
    if (socket) {
      socket.emit('send-message', {
        channelId: activeChannel,
        message,
        userId: profile?.id
      });
      setMessage('');
    }
  };

  return (
    <div className="h-full flex flex-col">
      <div className="flex-1">
        {messages.map((item, index) => (
          <MessageItem key={index} message={item} />
        ))}
      </div>
      <form className="flex gap-2" onSubmit={handleSubmit}>
        <input
          className="flex-1 border border-gray-200 p-2 rounded-md"
          value={message}
          onChange={(e) => setMessage(e.target.value)}
        />
        <button
          type="submit"
          disabled={!activeChannel}
          className="rounded-md text-white bg-gray-800 px-5 disabled:bg-gray-300 disabled:cursor-not-allowed">
          Send
        </button>
      </form>
    </div>
  );
};
