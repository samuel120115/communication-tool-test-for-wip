import axios from 'axios';
import { CreateChannel } from '@/types';

export const getChannels = () => {
  return axios.get('/channel');
};

export const getDetailedChannel = (id: string) => {
  return axios.get(`/channel/${id}`);
};

export const removeChannel = (id: string) => {
  return axios.delete(`/channel/${id}`);
};

export const createChannel = (payload: CreateChannel) => {
  return axios.post('/channel', payload);
};

export const removeUser = (payload: { userId: string; channelId: string }) => {
  return axios.post('/channel/remove-user', payload);
};

export const confirmChannelPassword = (payload: {
  id: string;
  password: string;
}) => {
  return axios.post('/channel/password', payload);
};

export const getMessagesInChannel = (channelId: string) => {
  return axios.get(`/message/${channelId}`);
};
