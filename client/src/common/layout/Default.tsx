import { Outlet, OutletProps } from 'react-router-dom';
import { Header } from '@/containers';
import { useProfileStore } from '@/stores/profile';

const DefaultLayout = (props: OutletProps) => {
  const { profile } = useProfileStore();

  if (!profile) {
    window.location.href = '/';
    return null;
  }

  return (
    <>
      <Header />

      <div className="container mx-auto">
        <Outlet {...props}></Outlet>
      </div>
    </>
  );
};

export default DefaultLayout;
