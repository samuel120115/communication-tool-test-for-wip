import { useProfileStore } from '@/stores/profile';
import React from 'react';

export const Header = () => {
  const { profile } = useProfileStore();

  return (
    <header className="container py-4 mx-auto">
      <div className="flex justify-between items-center">
        <h1 className="text-3xl font-bold">Chat</h1>
        <div className="flex-1 flex justify-end font-bold">
          <span>{profile?.name},&nbsp;</span>
          <span>{profile?.email}</span>
          {profile?.role === 'admin' && (
            <span className="text-xs p-1 rounded-md border border-gray-300 bg-gray-100 text-black mx-2">
              Admin
            </span>
          )}
        </div>
      </div>
    </header>
  );
};
