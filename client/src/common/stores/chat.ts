import { create } from 'zustand';
import { devtools } from 'zustand/middleware';
import { Socket } from 'socket.io-client';
import { Channel, ChannelInfo, Message } from '@/types';
import {
  getChannels,
  getDetailedChannel,
  getMessagesInChannel
} from '@/api/chat';

interface ChatState {
  socket: Socket | null;
  setSocket: (value: Socket) => void;
  channels: Channel[];
  activeChannel: string | null;
  setActiveChannel: (id: string | null) => void;
  messages: Message[];
  setMessages: (messages: Message[]) => void;
  detailedChannel: ChannelInfo | null;
  getDetailedChannelInfo: (id: string) => void;
  getChannels: () => Promise<void>;
  getMessages: (channelId: string) => Promise<void>;
}

export const useChatStore = create<ChatState>()(
  devtools(
    (set) => ({
      socket: null,
      channels: [],
      activeChannel: null,
      messages: [],
      detailedChannel: null,
      setMessages: (messages) => {
        set({ messages });
      },
      setActiveChannel: (value) => {
        set({ activeChannel: value });
      },
      setSocket: (value) => {
        set({ socket: value });
      },
      async getChannels() {
        try {
          const { data } = await getChannels();
          set({ channels: data.data });
        } catch (e) {
          console.log(e);
        }
      },
      async getMessages(channelId) {
        try {
          const { data } = await getMessagesInChannel(channelId);
          set({ messages: data.data });
        } catch (e) {
          console.log(e);
        }
      },
      async getDetailedChannelInfo(channelId: string) {
        try {
          const { data } = await getDetailedChannel(channelId);
          set({ detailedChannel: data.data });
        } catch (e) {
          console.log(e);
        }
      }
    }),
    {
      name: 'chat'
    }
  )
);
