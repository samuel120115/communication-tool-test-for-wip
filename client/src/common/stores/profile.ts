import { create } from 'zustand';
import { devtools, persist } from 'zustand/middleware';
import { User } from '@/types';
import { getUser } from '@/api/user';

interface TradersHubOverviewState {
  profile: User | null;
  setProfile: (profile: User) => void;
  getProfile: (id: string) => Promise<void>;
}

export const useProfileStore = create<TradersHubOverviewState>()(
  devtools(
    persist(
      (set) => ({
        profile: null,
        setProfile: (value) => {
          set({ profile: value });
        },

        async getProfile(id: string) {
          try {
            const { data } = await getUser(id);
            set({ profile: data.data });
          } catch (e) {
            console.log(e);
          }
        }
      }),
      {
        name: 'profile'
      }
    )
  )
);
