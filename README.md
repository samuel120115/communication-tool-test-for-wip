# communication-tool-test

Build a NodeJS app that allows multiple clients to communicate with each other in real time. The server process needs to support channels, so clients can have separate conversations. An admin REST API needs to be implemented in order to manage the creation/deletion of global channels, as well as getting the existing channels for clients to join.

## Basic requirements
- Server app (based on websockets + express for the REST API)
- Client app (web or NodeJS based - bare minimum)
- Support to see the list of users currently in the channel
- History of the last 10 messages in a channel when a new client joins and who sent them
- Statistics per channel with amount of messages ever sent since it was created, messages in the last 5 minutes and creation date of the channel

## Extended requirements
- Support for clients to create their own channels
- Support for password protecting channels
- Support for RBAC(standard users and admins) where admins can delete user channels, remove people from channels and delete messages

## Advanced requirements
- Support for secure websockets
- Support for WebRTC(web clients) audio streams for conferencing
